package com.example.movil_1;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class monedaActivity extends AppCompatActivity {

    private Spinner spnMoneda;
    private EditText txtMoneda;
    private Button btnCalcularMoneda, btnLimpiarMoneda, btnCerrarMoneda;
    private TextView txtResultadoMoneda, txtTipoMoneda;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_moneda);

        iniciarComponentes();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.srcMonedas, android.R.layout.simple_spinner_item);

        spnMoneda.setAdapter(adapter);

        btnCalcularMoneda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int seleccion = spnMoneda.getSelectedItemPosition();
                if (txtMoneda.getText().toString().matches("")){
                    Toast.makeText(monedaActivity.this, "Ingrese una cantidad de monedas",
                            Toast.LENGTH_SHORT).show();
                } else if (seleccion == 0) {

                    Toast.makeText(monedaActivity.this, "Seleccione un tipo de moneda",
                            Toast.LENGTH_SHORT).show();
                } else{
                    float cantidadMoneda = Float.parseFloat(txtMoneda.getText().toString());
                    switch (seleccion) {
                        case 1: {
                            float resultadoMoneda = (float) (cantidadMoneda * 0.06);
                            txtResultadoMoneda.setText(String.valueOf(resultadoMoneda));
                            txtTipoMoneda.setText("USD");
                            break;
                        }
                        case 2: {
                            float resultadoMoneda = (float) (cantidadMoneda * 0.055);
                            txtResultadoMoneda.setText(String.valueOf(resultadoMoneda));
                            txtTipoMoneda.setText("EUR");
                            break;
                        }
                        case 3:{
                            float resultadoMoneda = (float) (cantidadMoneda * 0.082);
                            txtResultadoMoneda.setText(String.valueOf(resultadoMoneda));
                            txtTipoMoneda.setText("CAD");
                            break;
                        }
                        case 4:{
                            float resultadoMoneda = (float) (cantidadMoneda * 0.047);
                            txtResultadoMoneda.setText(String.valueOf(resultadoMoneda));
                            txtTipoMoneda.setText("GBP363636");
                            break;
                        }
                        default:
                            Toast.makeText(monedaActivity.this, "Hubo un error", Toast.LENGTH_SHORT).show();
                            return;
                    }
                }

            }
        });

        btnLimpiarMoneda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spnMoneda.setSelection(0);
                txtMoneda.setText("");
                txtResultadoMoneda.setText("000.000");
                Toast.makeText(monedaActivity.this, "Monedas reestablecidas",
                        Toast.LENGTH_SHORT).show();
            }
        });

        btnCerrarMoneda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        spnMoneda = (Spinner) findViewById(R.id.spnMoneda);
        txtMoneda = (EditText) findViewById(R.id.txtMoneda);
        txtResultadoMoneda = (TextView) findViewById(R.id.txtResultadoMoneda);
        txtTipoMoneda = (TextView) findViewById(R.id.txtTipoMoneda);
        btnCalcularMoneda = (Button) findViewById(R.id.btnCalcularMoneda);
        btnLimpiarMoneda = (Button) findViewById(R.id.btnLimpiarMoneda);
        btnCerrarMoneda = (Button) findViewById(R.id.btncerrarMoneda);

    }
}